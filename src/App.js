import React from "react"; // node_modules配下にあるライブラリは名前だけ
import {BrowserRouter, Route, Switch, Link} from "react-router-dom";
import {Toolbar} from "@material-ui/core";

import LoginForm from "./login-form"; // 自作したコンポーネントは相対パスで指定
import ItemList from "./item-list";
import Home from './home';

const App = (props) => {
    return (
        <BrowserRouter>
            <div>
                <Toolbar>
                    {/* aタグを使う代わりにLinkタグを使ってリンクを設置 */}
                    <Link to='/'>トップ</Link>-
                    <Link to='/login'>ログイン</Link>-
                    <Link to='/list'>一覧</Link>
                    {/* to属性でこのリンクをクリックした際の遷移先URLを指定 */}
                </Toolbar>
                {/* Switchコンポーネントで指定されたURLに合致するコンポーネントにページが
                 切り替わるようにする */}
                <Switch>
                    {/* Routeコンポーネントでルーティングの設定を行う */}
                    <Route exact path='/' component={Home} />
                    <Route path='/login' component={LoginForm} />
                    <Route path='/list' component={ItemList} />
                </Switch>
            </div>
        </BrowserRouter>
    );
};
export default App;
