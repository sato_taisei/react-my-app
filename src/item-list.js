import React, { useState} from "react";
import { Checkbox, List, ListItem, ListItemText} from "@material-ui/core";

// 一覧するデータを状態（state）として設定
const initialItems = [
    // "contents"の内容がListコンポーネントに表示される
    // チェックボックスをチェックすると"checked"が変更される
    {id: 1, contents: 'やること１', checked: false},
    {id: 2, contents: 'やること２', checked: false},
    {id: 3, contents: 'やること３', checked: false},
    {id: 4, contents: 'やること４', checked: false},
];

const ItemList = () => {
    const [items, setItems] = useState(initialItems);
    const toggleChecked = (item) => {
        // itemはリスト内の１行分のデータ
        // 戻り値はチェックボックスをON/OFFした際に動作する関数
        return () => {
            const targetIndex = items.indexOf(item);
            // 対象データのチェック状態を変更
            items[targetIndex].checked = !items[targetIndex].checked;
            // 状態(state)を、変更後の一覧データと変更用の関数を使って更新
            setItems([...items]);
        };
    }
    return (
        <List>
            {items.map(item => (
                    <ListItem key={item.id}>
                        <Checkbox
                            checked={item.checked}
                            onChange={toggleChecked(item)}
                        />
                        <ListItemText primary={item.contents}/>
                    </ListItem>
            ))}
    </List>
    );
};
export default ItemList