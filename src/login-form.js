import React from 'react';
import { Button, Paper, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

// styleオブジェクトを返すフックを作成
const useStyles = makeStyles({
    container: {
        display: 'flex', height: '100vh', backgroundColor: '#eeeeee'
    },
    loginBase: {
        width: '300px', height: '200px', margin: 'auto', padding: '10px'
    },
    field: { width: '80%' },
    loginButton: { margin: '15px'}
});

// 自作するコンポーネントは基本的に先頭大文字！！
const LoginForm = () => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <Paper className={classes.loginBase}>
                <Typography>ログインしてください。</Typography>
                <TextField className={classes.field} label='Name' />
                <TextField className={classes.field}　label='Password' />
                <Button className={classes.loginButton}
                        variant='contained' color='primary'>
                    ログイン
                </Button>
            </Paper>
        </div>
    );
};
export default LoginForm;